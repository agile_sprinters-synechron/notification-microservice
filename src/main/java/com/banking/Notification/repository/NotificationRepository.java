package com.banking.Notification.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.banking.Notification.model.Notification;


public interface NotificationRepository extends JpaRepository<Notification, Integer>{

	Optional<List<Notification>> findBycustomerid(Integer customerid);

	Optional<Notification> findByIdAndCustomerid(Integer id, Integer customerid);

}
