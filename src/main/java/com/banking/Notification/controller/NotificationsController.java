package com.banking.Notification.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.banking.Notification.mail.Mail;
import com.banking.Notification.mail.MailService;
import com.banking.Notification.model.Notification;
import com.banking.Notification.service.NotificationService;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;


@RestController
public class NotificationsController {

	private static final Logger log = LoggerFactory.getLogger(NotificationsController.class);

	@Autowired
	private NotificationService service;

    @Autowired
	private MailService mailService;
   
	
	@GetMapping("/api/customer/{customerid}")
	@ResponseBody
	public ResponseEntity<Object> listBycustomerid(@PathVariable Integer customerid) {

		Optional<List<Notification>> notificationData = service.listAllBycustomerid(customerid);

		if (notificationData.isPresent()) {
			if (!notificationData.get().isEmpty()) {
				log.info(notificationData.get().size() + " notifications present for CustomerID:" + customerid);
				return ResponseEntity
			            .status(HttpStatus.OK)                 
			            .body(notificationData.get());
			} else {
				log.error("Notification not present for CustomerID:" + customerid);
				 return ResponseEntity
				            .status(HttpStatus.NOT_FOUND)
				            .body("Notifications not present for CustomerID:" + customerid);
			}
		} else {
			log.error("Notifications not present for CustomerID:" + customerid);
			 return ResponseEntity
			            .status(HttpStatus.NOT_FOUND)
			            .body("notifications not present for CustomerID:" + customerid);
		}

	}

	@GetMapping("/api/customer/{customerid}/notifications/{id}")
	@ResponseBody
	public ResponseEntity<Object> list(@PathVariable Integer customerid, @PathVariable Integer id) {
		Optional<Notification> notificationData = service.findByIdAndCustomerid(id, customerid);

		if (notificationData.isPresent()) {
			log.info("notification present for: " + id + " for CustomerID " + customerid);
			return ResponseEntity
		            .status(HttpStatus.OK)                 
		            .body(notificationData.get());
			
		} else {
			log.error("No notification present with ID:" + id + " for CustomerID " + customerid);
			 return ResponseEntity
		            .status(HttpStatus.NOT_FOUND)
		            .body("No notification present with ID:" + id + " for CustomerID " + customerid);
		}

	}

	@PostMapping("/api/notifications/customer/{customerid}")
	public ResponseEntity<Object> add(@RequestBody Notification notification, @PathVariable Integer customerid) {

		try {
			Notification notificationData = service.save(new Notification(notification.getId(), customerid, notification.getEmail_address(),
					notification.getMessage(), notification.getPhone()));
		/*
			 Mail mail = new Mail();
		        mail.setMailFrom("pranaypednekar1996@gmail.com");
		        mail.setMailTo(notification.getEmail_address());
		        mail.setMailSubject("Notifications");
		        mail.setMailContent(notification.getMessage());
		        mailService.sendEmail(mail);
			
		        */
		        Twilio.init("AC9fd44f74bc0e45ba4fe2501a0f8d9a0d", "dda612572aae35a88644e0ce88006e8d");
		       Message.creator(
		            new PhoneNumber(notification.getPhone()),
		            new PhoneNumber("+12055309078"),
		            notification.getMessage())
		        .create();
		        
		        
			log.info("notification sent for CustomerID:" + customerid);
			return ResponseEntity
		            .status(HttpStatus.CREATED)                 
		            .body(notificationData);
			
		} catch (Exception e) {
			  e.printStackTrace();
			log.error("Error in sending notifications for CustomerID  " + customerid);
			 return ResponseEntity
			            .status(HttpStatus.INTERNAL_SERVER_ERROR)
			            .body("Error in sending notifications for CustomerID " + customerid);
		}

	}
	
	
}
