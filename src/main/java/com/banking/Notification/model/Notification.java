package com.banking.Notification.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Notification {

	private Integer id;
	private Integer customerid;
	private String email_address;
	private String message;
	private String phone;

	public Notification() {
	}

	public Notification(Integer id, Integer customerid, String email_address, String message, String phone) {
		this.id = id;
		this.customerid = customerid;
		this.email_address = email_address;
		this.message = message;
		this.phone = phone;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomerid() {
		return customerid;
	}

	public void setCustomerid(Integer customerid) {
		this.customerid = customerid;
	}

	public String getEmail_address() {
		return email_address;
	}

	public void setEmail_address(String email_address) {
		this.email_address = email_address;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Notification [id=" + id + ", customerid=" + customerid + ", email_address=" + email_address
				+ ", message=" + message + ", phone=" + phone + "]";
	}


	
	
}
