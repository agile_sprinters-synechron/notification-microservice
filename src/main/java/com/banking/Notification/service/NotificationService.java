package com.banking.Notification.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banking.Notification.model.Notification;
import com.banking.Notification.repository.NotificationRepository;



@Service
@Transactional
public class NotificationService {
	
	 @Autowired
	    private NotificationRepository repo;
	     
	    public Optional<List<Notification>> listAllBycustomerid(Integer customerid) {
	        return repo.findBycustomerid(customerid);
	    }
	     
	    public Notification save(Notification notification) {
	        return repo.save(notification);
	    }
	     
	    public Optional<Notification> findByIdAndCustomerid(Integer id,Integer customerid) {
	        return repo.findByIdAndCustomerid(id,customerid);
	    }
	     

}
